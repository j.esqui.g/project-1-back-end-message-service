// import {Datastore} from '@google-cloud/datastore';
// import express from 'express';
// import cors from 'cors'

const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')
const axios = require('axios');
const { response } = require('express');

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()

let idCount = 0;

app.get('/messages/:mid', async(req,res)=>{
    const messageID = String(req.params.mid)
    const query = datastore.createQuery('Message').filter('id','=',messageID);
    const result = await datastore.runQuery(query)
    res.send(result[0])
})
// GET /messages
app.get('/messages', async(req,res)=>{

    if(req.query.sender && req.query.recipient)
    {
        const sender = String(req.query.sender)
        const recipient = String(req.query.recipient)
        const query = datastore.createQuery('Message').filter('sender','=',sender).filter('recipient','=',recipient) // get messages from specified sender and recipient
        const result = await datastore.runQuery(query)
        res.send(result[0])
    }
    else if(req.query.sender)
    {
        const sender = String(req.query.sender)
        const query = datastore.createQuery('Message').filter('sender','=',sender) // get messages from recipient
        const [data,metaInfo] = await datastore.runQuery(query)
        res.send(data)
    }
    else if(req.query.recipient)
    {
        const recipient = String(req.query.recipient)
        const query = datastore.createQuery('Message').filter('recipient','=',recipient) // get messages from recipient
        const [data,metaInfo] = await datastore.runQuery(query)
        res.send(data)
    }
    else
    {
        const query = datastore.createQuery('Message')// get all messages
        const result = await datastore.runQuery(query)
        res.send(result[0])
    }

})

app.post('/messages', async(req,res)=>{


    const body = req.body;

    const senderExists = await axios.get(`https://wedding-planner-325216.uk.r.appspot.com/users/${body.sender}/verify`)

    if(senderExists.data === "User Found")
    {
        const recipientExists = await axios.get(`https://wedding-planner-325216.uk.r.appspot.com/users/${body.recipient}/verify`)

        if(recipientExists.data === "User Found")
        {
            const currentdate = new Date();
             const datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth()+1)  + "/" 
                        + currentdate.getFullYear() + " @ "  
                        + currentdate.getHours() + ":"  
                        + currentdate.getMinutes() + ":" 
                        + currentdate.getSeconds();
                        
            const body = req.body;
            const message = {id: `${++idCount}`, sender: `${body.sender}`, recipient: `${body.recipient}`, note: `${body.note}`, timestamp: `${datetime}` }
            const key = datastore.key(['Message']) // use auto generated id
            const response = await datastore.save({key:key,data:message})
            res.status(201)
            res.send('Successfully created a message')
        }
        else
        {
            res.send("no user")
        }
    }
    else{
        res.send("no user")
    }

})

const PORT = process.env.PORT ||8080;
app.listen(PORT,()=>console.log("Messaging Application has started"));